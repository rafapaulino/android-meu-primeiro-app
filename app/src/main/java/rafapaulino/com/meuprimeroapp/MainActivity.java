package rafapaulino.com.meuprimeroapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView texto2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView objetoTexto = (TextView) findViewById(R.id.campoTexto);
        objetoTexto.setText("Nova mensagem legal!!!");

        Button objetoBotao = (Button) findViewById(R.id.meuBotao);
        objetoBotao.setText("Label Botão");

        objetoBotao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objetoTexto.setText("Curso Android Studio!!!");
            }
        });

        Button botaoComLabelAlteradoStringGeral = (Button) findViewById(R.id.buttonProgramacao2);
        botaoComLabelAlteradoStringGeral.setText(R.string.rotuloBotao);

        texto2 = (TextView) findViewById(R.id.texto2);
    }

    public void clicouBotao(View botao) {
        texto2.setText("Botão Clicado!!!");
    }
}
